var request = require('request');
var config = require('config');
var _ = require('underscore');
module.exports = function(app) {
    app.get('/dogs', function(req,res,next) {
        var url = config.get('host') + "api/breeds/list/all";
        console.log(url);
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var jsPar = JSON.parse(body);
                var obj = {};
                var list = [];
                console.log(_.size(jsPar.message));
                _.each(jsPar, function(item){
                    //contain status & message
                    var key = _.keys(item);
                    var value = _.values(item);
                    
                    if (key.length != 0 && value.length !=0){
                        // console.log("----------------------Item ----------------------------- \n");
                        // console.log(_.keys(item));
                        // console.log("----------------------Value ----------------------------- \n");
                        // console.log(_.values(item));
                        // var len = _.size(jsPar.message);
                        // console.log(len);
                        for(var i=0, len = _.size(jsPar.message); i<len; i++){
                            var breed = _.keys(item)[i];
                            var subreed = _.values(item)[i];
                            var objSub = {}
                            var listSub = [];
                            for (var j=0, subLen=subreed.length; j<subLen; j++) {
                                objSub = {
                                    breed: subreed[j],
                                    sub_breed : []
                                };
                                listSub.push(objSub);
                            }
                            obj = {
                                breed : breed,
                                sub_breed : listSub
                            };
                            list.push(obj);
                        }
                    }
                    
                });
                
                res.json(list);
            } else {
                res.json(JSON.parse(error));
            }
        });
    });

    function dogDetil(req, res, next) {
        var param = req.params.subBreed;
        var listSub = [];
        var url = config.get('host') + "api/breed/" + param + "/list";
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var jsPar = JSON.parse(body);
                _.each(jsPar, function(item){
                    var key = _.keys(item);
                    var value = _.values(item);
                    if (key.length != 0 && value.length !=0){
                        for(var i=0, len = _.size(jsPar.message); i<len; i++){
                            var breed = _.values(item)[i];
                            objSub = {
                                breed : breed,
                                sub_breed : null
                            };
                            listSub.push(objSub);
                        }
                    }
                });
                req.dogDetil = listSub;
                next();
            } else {
                res.json(error);
            }
        });
    }

    function dogImg(req, res, next) {
        var param = req.params.subBreed;
        var urlImg = config.get('host') + "api/breed/" + param + "/images";
        console.log(urlImg);
        request(urlImg, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                req.dogImg = JSON.parse(body).message;
                next();
            } else {
                res.json(error);
            }
        });
        
    }

    app.get('/dogs/:subBreed', dogDetil, dogImg, function(req,res,next) {
        var param = req.params.subBreed;
        var obj = {};
        obj = {
            bread : param,
            sub_breed : req.dogDetil,
            images : req.dogImg
        };
        res.json(obj);
    });

    

}
var request = require('request');
var config = require('config');
module.exports = function(app) {
    app.get('/breeds/list/all', function(req,res,next) {
        var url = config.get('host') + "api/breeds/list/all";
        console.log(url);
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                res.json(JSON.parse(body));
            } else {
                res.json(JSON.parse(error));
            }
        });
    });

    app.get('/breed/:subBreed/list', function(req,res,next) {
        var param = req.params.subBreed;
        console.log(param);
        var url = config.get('host') + "api/breed/" + param + "/list";
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                res.json(JSON.parse(body));
            } else {
                res.json(error);
            }
        });
    });

    

}